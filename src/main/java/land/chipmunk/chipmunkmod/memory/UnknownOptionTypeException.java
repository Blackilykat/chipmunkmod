package land.chipmunk.chipmunkmod.memory;

public class UnknownOptionTypeException extends RuntimeException { //runtime exception because i dont wanna catch this i just want it to crash me when something goes wrong
    public UnknownOptionTypeException(Class<?> type) {
        super(String.format("An option with type %s is not handled by ModuleMemory.", type.getTypeName()));
    }
}

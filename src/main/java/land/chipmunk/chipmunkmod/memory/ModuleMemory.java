package land.chipmunk.chipmunkmod.memory;

import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.Configuration;
import land.chipmunk.chipmunkmod.testclient.gui.Gui;
import land.chipmunk.chipmunkmod.testclient.gui.components.Category;
import land.chipmunk.chipmunkmod.testclient.gui.components.Module;
import land.chipmunk.chipmunkmod.testclient.gui.components.Option;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import static land.chipmunk.chipmunkmod.ChipmunkMod.LOGGER;

// i have realised this can also be used to save profiles
// like it would be so simple to implement that feature
// not 1.1.0 tho it's taking too long
// whyt he fuck is this here lmao i have a working config jsut use that ??1'11'!?
public class ModuleMemory {

    public static class Module {
        public final String name;
        public boolean enabled;
        public ArrayList<Option<?>> options = new ArrayList<>();
        public Module(String name, boolean enabled) {
            this.name = name;
            this.enabled = enabled;
        }
    }

    public static class Option<T extends Object> {
        public final String name;
        public T value;
        public Option(String name, T value) {
            this.name = name;
            this.value = value;
        }
        public Class<T> getType() {return (Class<T>) value.getClass();} // ignore warning cause intellij has the stupid
    }

    public static void load() {
        for (Map.Entry<String, Boolean> categoryInMemory : ChipmunkMod.CONFIG.memory.categories.entrySet()) {
            for (land.chipmunk.chipmunkmod.testclient.gui.components.Category categoryInGui : Gui.categoryList) {
                if(categoryInMemory.getKey().equals(categoryInGui.getMessage().getString())) {
                    categoryInGui.isExtended = categoryInMemory.getValue();
                }
            }
        }
        for (Map.Entry<String, Boolean> moduleInMemory : ChipmunkMod.CONFIG.memory.modules.entrySet()) {

            String[] split = moduleInMemory.getKey().split("\\.");
            String category = split[0];
            String module = split[1];

            for (land.chipmunk.chipmunkmod.testclient.gui.components.Category categoryInGui : Gui.categoryList) {
                if(!categoryInGui.getMessage().getString().equals(category)) continue;

                for (land.chipmunk.chipmunkmod.testclient.gui.components.Module moduleInGui : categoryInGui.moduleList) {
                    if(module.equals(moduleInGui.getMessage().getString())) {
                        moduleInGui.isEnabled = moduleInMemory.getValue();
                    }
                }
            }
        }
        for (Map.Entry<String, Boolean> moduleInMemory : ChipmunkMod.CONFIG.memory.modules.entrySet()) {

            String[] split = moduleInMemory.getKey().split("\\.");
            String category = split[0];
            String module = split[1];

            for (land.chipmunk.chipmunkmod.testclient.gui.components.Category categoryInGui : Gui.categoryList) {
                if(!categoryInGui.getMessage().getString().equals(category)) continue;

                for (land.chipmunk.chipmunkmod.testclient.gui.components.Module moduleInGui : categoryInGui.moduleList) {
                    if(module.equals(moduleInGui.getMessage().getString())) {
                        moduleInGui.isEnabled = moduleInMemory.getValue();
                    }
                }
            }
        }
        for (Map.Entry<String, String> optionInMemory : ChipmunkMod.CONFIG.memory.options.entrySet()) {

            String[] split = optionInMemory.getKey().split("\\.");
            String category = split[0];
            String module = split[1];
            String option = split[2];

            for (land.chipmunk.chipmunkmod.testclient.gui.components.Category categoryInGui : Gui.categoryList) {
                if(!categoryInGui.getMessage().getString().equals(category)) continue;

                for (land.chipmunk.chipmunkmod.testclient.gui.components.Module moduleInGui : categoryInGui.moduleList) {
                    if(!module.equals(moduleInGui.getMessage().getString())) continue;

                    for (land.chipmunk.chipmunkmod.testclient.gui.components.Option<?> optionInGui : moduleInGui.optionList) {
                        if(option.equals(optionInGui.name)) {
                            optionInGui.setValueFromString(optionInMemory.getValue());
                        }
                    }
                }
            }
        }

        LOGGER.info("Loaded module memory!");
    }

    public static void save() {
        for (land.chipmunk.chipmunkmod.testclient.gui.components.Category categoryInGui : Gui.categoryList) {
            String categoryKey = categoryInGui.getMessage().getString();
            ChipmunkMod.CONFIG.memory.categories.put(categoryKey, categoryInGui.isExtended);

            for (land.chipmunk.chipmunkmod.testclient.gui.components.Module moduleInGui : categoryInGui.moduleList) {
                String moduleKey = categoryKey + "." + moduleInGui.getMessage().getString();
                ChipmunkMod.CONFIG.memory.modules.put(moduleKey, moduleInGui.isEnabled);

                for (land.chipmunk.chipmunkmod.testclient.gui.components.Option<?> optionInGui : moduleInGui.optionList) {
                    String key = moduleKey + "." + optionInGui.name;
                    ChipmunkMod.CONFIG.memory.options.put(key, optionInGui.getValueAsString());
                }

            }
        }
    }
}

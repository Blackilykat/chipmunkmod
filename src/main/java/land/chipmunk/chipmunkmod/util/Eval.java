package land.chipmunk.chipmunkmod.util;

import jdk.jshell.JShell;

public class Eval {
    public static JShell shell = JShell.create();
    public static void shell(String code) {
        shell.eval(code);
    }
    static {
        shell.onSnippetEvent(event -> {
            Chat.sendGold(event.value());
        });
    }


}

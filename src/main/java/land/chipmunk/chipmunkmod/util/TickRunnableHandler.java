package land.chipmunk.chipmunkmod.util;

import land.chipmunk.chipmunkmod.testclient.gui.Gui;
import land.chipmunk.chipmunkmod.testclient.gui.components.Category;
import land.chipmunk.chipmunkmod.testclient.gui.components.Module;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.minecraft.client.MinecraftClient;

import java.util.ArrayList;

public class TickRunnableHandler {
	public static ArrayList<Runnable> runAtTickEnd = new ArrayList<>();
	
	public static void registerTickEndRunnables() {
		ClientTickEvents.END_CLIENT_TICK.register(client -> {
			for (Category category : Gui.categoryList) {
				for (Module module : category.moduleList) {
					if(module.isEnabled) {
						if(
								(!module.needsInWorld || MinecraftClient.getInstance().player != null)
										&& module.endTickRunnable != null
						) module.endTickRunnable.run();
					}
				}
			}
		});
	}
}

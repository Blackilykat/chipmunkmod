package land.chipmunk.chipmunkmod.util;

import net.minecraft.server.MinecraftServer;

/**
 * This class is used when there isn't a specific class where you'd store a variable, but you still need it somewhere.
 * It's mostly used by mixins which can't hold public variables themselves.
 */
public class SharedVariables {
	public static MinecraftServer serverConnectedTo = null;
	public static int elderGuardianParticleTimer = 0;
}

package land.chipmunk.chipmunkmod.util;

import land.chipmunk.chipmunkmod.ChipmunkMod;

import java.util.ArrayList;

public class Debug {
    public static ArrayList<String> known = new ArrayList<>();
    public static ArrayList<String> selected = new ArrayList<>();
    public static void debug(String string, String caller) {
        if(selected.contains(caller)) ChipmunkMod.LOGGER.info(String.format("[DEBUG|%s] %s", caller, string));
        if(!known.contains(caller)) known.add(caller);
    }
}

package land.chipmunk.chipmunkmod.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Executor {
    public static ExecutorService service = Executors.newFixedThreadPool(20);
    public static ExecutorService antiChatSpamService = Executors.newFixedThreadPool(1);
    public static Future<?> submit(Runnable runnable) {
        return service.submit(runnable);
    }
}

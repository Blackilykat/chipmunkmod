package land.chipmunk.chipmunkmod.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import land.chipmunk.chipmunkmod.util.Chat;
import land.chipmunk.chipmunkmod.util.Executor;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;

import java.util.List;
import java.util.concurrent.Executors;

import static land.chipmunk.chipmunkmod.command.CommandManager.literal;

public class ClearAntiChatSpamQueueCommand {
    public static void register (CommandDispatcher<FabricClientCommandSource> dispatcher) {
        final ClearAntiChatSpamQueueCommand instance = new ClearAntiChatSpamQueueCommand();
        dispatcher.register(
                literal("clearantichatspamqueue")
                        .executes(instance::run)
        );
    }

    public int run(CommandContext<FabricClientCommandSource> context) throws CommandSyntaxException {
        List<Runnable> runnables = Executor.antiChatSpamService.shutdownNow();
        Executor.antiChatSpamService = Executors.newFixedThreadPool(1);
        Chat.sendGreen("Stopped " + runnables.size() + " chat messages from getting recieved!");
        return 1;
    }
}

package land.chipmunk.chipmunkmod.commands;

import com.mojang.brigadier.CommandDispatcher;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.minecraft.text.Text;

import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static land.chipmunk.chipmunkmod.command.CommandManager.argument;
import static land.chipmunk.chipmunkmod.command.CommandManager.literal;
import static land.chipmunk.chipmunkmod.util.BotValidationUtilities.*;

public class ValidateCommand {
  public static void register (CommandDispatcher<FabricClientCommandSource> dispatcher) {
    dispatcher.register(
      literal("validate")
        .then(literal("hbot").then(argument("command", greedyString()).executes(c -> hbot(getString(c, "command")))))
        .then(literal("sbot").then(argument("command", greedyString()).executes(c -> sbot(getString(c, "command")))))
        // .then(literal("chipmunk").then(argument("command", greedyString()).executes(c -> chipmunk(getString(c, "command")))))
        .then(literal("chomens").then(argument("command", greedyString()).executes(c -> {
          c.getSource().sendFeedback(Text.literal("Warning: Manual ChomeNS Bot validation is deprecated. Please use the completions from typing the bot's prefix."));

          return chomens(getString(c, "command"));
        })))
        .then(literal("fnfboyfriend").then(argument("command", greedyString()).executes(c -> fnfboyfriend(getString(c, "command")))))
              .then(literal("nbot").then(argument("command", greedyString()).executes(c -> nbot(getString(c, "command")))))
        .then(literal("kittycorp").then(argument("command", greedyString()).executes(c -> kittycorp(getString(c, "command")))))
    );
  }
}

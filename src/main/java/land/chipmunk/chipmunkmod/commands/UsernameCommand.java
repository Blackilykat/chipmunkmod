package land.chipmunk.chipmunkmod.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import static land.chipmunk.chipmunkmod.command.CommandManager.literal;
import static land.chipmunk.chipmunkmod.command.CommandManager.argument;

import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.util.SharedVariables;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.TitleScreen;
import net.minecraft.client.gui.screen.multiplayer.ConnectScreen;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.client.network.ServerAddress;
import net.minecraft.client.session.Session;
import net.minecraft.text.Text;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;
import java.util.Optional;
import land.chipmunk.chipmunkmod.mixin.MinecraftClientAccessor;

public class UsernameCommand {
  private static final SimpleCommandExceptionType USERNAME_TOO_LONG = new SimpleCommandExceptionType(Text.translatable("The specified username is longer than 16 characters"));

  public static void register (CommandDispatcher<FabricClientCommandSource> dispatcher) {
    dispatcher.register(
      literal("username")
        .then(
          literal("set")
            .then(
              argument("username", greedyString())
                .executes(c -> updateUsername(c))
            )
        )
    );
  }

  public static int updateUsername (CommandContext<FabricClientCommandSource> context) throws CommandSyntaxException {
    final String username = getString(context, "username");
    if (username.length() > 16) throw USERNAME_TOO_LONG.create();
    return updateSession(context, username);
  }

  public static int updateSession (CommandContext<FabricClientCommandSource> context, String username) throws CommandSyntaxException {
    final FabricClientCommandSource source = context.getSource();

    final MinecraftClient client = source.getClient();

    ChipmunkMod.CONFIG.defaultUsername = username;

    // TODO: Put this in a separate class
    final ServerInfo info = client.getCurrentServerEntry();
    client.world.disconnect();
    client.disconnect();
    ConnectScreen.connect(new TitleScreen(), client, ServerAddress.parse(info.address), info, false, null);

    return Command.SINGLE_SUCCESS;
  }
}

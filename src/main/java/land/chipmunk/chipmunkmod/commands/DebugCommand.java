package land.chipmunk.chipmunkmod.commands;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.suggestion.SuggestionProvider;
import land.chipmunk.chipmunkmod.util.Debug;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

import java.util.ArrayList;

import static land.chipmunk.chipmunkmod.command.CommandManager.literal;
import static land.chipmunk.chipmunkmod.command.CommandManager.argument;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static com.mojang.brigadier.arguments.StringArgumentType.string;

public class DebugCommand {
    public static SuggestionProvider<FabricClientCommandSource> knownSuggestions = (context, builder) -> {
        Debug.known.stream()
                .filter(option -> option.startsWith(builder.getRemaining().toLowerCase()))
                .forEach(builder::suggest);
        return builder.buildFuture();
    };
    public static SuggestionProvider<FabricClientCommandSource> selectedSuggestions = (context, builder) -> {
        Debug.selected.stream()
                .filter(option -> option.startsWith(builder.getRemaining().toLowerCase()))
                .forEach(builder::suggest);
        return builder.buildFuture();
    };
    public static void register(CommandDispatcher<FabricClientCommandSource> dispatcher) {
        dispatcher.register(
                literal("debug")
                        .then(literal("add")
                                .then(argument("caller", string())
                                        .suggests(knownSuggestions)
                                        .executes(DebugCommand::add)))
                        .then(literal("remove")
                                .then(argument("caller", string())
                                        .suggests(selectedSuggestions)
                                        .executes(DebugCommand::remove)))
                        .then(literal("clear")
                                .executes(DebugCommand::clear))
        );
    }

    public static int add(CommandContext<FabricClientCommandSource> context) {
        String caller = context.getArgument("caller", String.class);
        Debug.selected.add(caller);
        context.getSource().sendFeedback(Text.literal("Added ").append(Text.literal(caller).formatted(Formatting.YELLOW)).append(Text.literal(" to the debug list!")));
        return 0;
    }
    public static int remove(CommandContext<FabricClientCommandSource> context) {
        String caller = context.getArgument("caller", String.class);
        Debug.selected.remove(caller);
        context.getSource().sendFeedback(Text.literal("Removed ").append(Text.literal(caller).formatted(Formatting.YELLOW)).append(Text.literal(" from the debug list!")));
        return 0;
    }
    public static int clear(CommandContext<FabricClientCommandSource> context) {
        Debug.selected = new ArrayList<>();
        context.getSource().sendFeedback(Text.literal("Cleared the debug list!"));
        return 0;
    }
}

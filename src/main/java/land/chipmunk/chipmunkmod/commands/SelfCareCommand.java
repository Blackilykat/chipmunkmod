package land.chipmunk.chipmunkmod.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.context.CommandContext;
import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.modules.SelfCare;
import land.chipmunk.chipmunkmod.testclient.modules.anti_annoyances.SelfCareModule;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.network.PlayerListEntry;
import net.minecraft.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static com.mojang.brigadier.arguments.BoolArgumentType.bool;
import static com.mojang.brigadier.arguments.BoolArgumentType.getBool;
import static land.chipmunk.chipmunkmod.command.CommandManager.argument;
import static land.chipmunk.chipmunkmod.command.CommandManager.literal;

public class SelfCareCommand {
    private static final Logger log = LoggerFactory.getLogger(SelfCareCommand.class);

    public static void register (CommandDispatcher<FabricClientCommandSource> dispatcher) {
        dispatcher.register(
                literal("selfcare")
                        .then(
                                literal("op")
                                        .then(
                                                argument("boolean", bool())
                                                        .executes(m -> setSelfCare(m, "op"))
                                        )
                        )
                        .then(
                                literal("gamemode")
                                        .then(
                                                argument("boolean", bool())
                                                        .executes(m -> setSelfCare(m, "gamemode"))
                                        )
                        )
                        .then(
                                literal("cspy")
                                        .then(
                                                argument("boolean", bool())
                                                        .executes(m -> setSelfCare(m, "cspy"))
                                        )
                        )
                        .then(
                                literal("icu")
                                        .then(
                                                argument("boolean", bool())
                                                        .executes(m -> setSelfCare(m, "icu"))
                                        )
                        )
                        .then(
                                literal("skin")
                                        .then(
                                                argument("skin", StringArgumentType.string())
                                                        .executes(context -> {
                                                            final FabricClientCommandSource source = context.getSource();
                                                            final String skin = StringArgumentType.getString(context, "skin");

                                                            SelfCare.INSTANCE.hasSkin = false;
                                                            SelfCare.INSTANCE.skin = skin;
                                                            if(skin.equals("off")) {
                                                                SelfCareModule.INSTANCE.skinOption.setOptionValue(false);
                                                                source.sendFeedback(Text.literal("Disabled skin self care"));
                                                            } else {
                                                                SelfCareModule.INSTANCE.skinOption.setOptionValue(true);
                                                                SelfCareModule.INSTANCE.skinUsernameOption.setOptionValue(skin);
                                                                source.sendFeedback(Text.literal("Skin self care enabled for " + skin + "'s skin"));
                                                            }

                                                            return Command.SINGLE_SUCCESS;
                                                        })
                                                        .suggests((context, builder) -> {
                                                            List<String> possibilities = new ArrayList<>();
                                                            possibilities.add("off");

                                                            String currentContext;
                                                            try {
                                                                currentContext = StringArgumentType.getString(context, "skin");
                                                            } catch(IllegalArgumentException e) {
                                                                currentContext = "";
                                                            }
                                                            ClientPlayNetworkHandler networkHandler = MinecraftClient.getInstance().getNetworkHandler();
                                                            if(networkHandler == null) {
                                                                // probably impossible, but can't hurt having this
                                                                possibilities.add(ChipmunkMod.CONFIG.defaultUsername);
                                                            } else {
                                                                for(PlayerListEntry player : networkHandler.getPlayerList()) {
                                                                    log.info("Found possibility {}", player.getProfile().getName());
                                                                    possibilities.add(player.getProfile().getName());
                                                                }
                                                            }

                                                            for(String possibility : possibilities) {
                                                                if(possibility.toLowerCase().startsWith(currentContext.toLowerCase())) {
                                                                    builder.suggest(possibility);
                                                                }
                                                            }
                                                            return builder.buildFuture();
                                                        })
                                        )
                        )
        );
    }

    // setSelfCare is probably not a good name for this
    public static int setSelfCare (CommandContext<FabricClientCommandSource> context, String type) {
        final FabricClientCommandSource source = context.getSource();
        final boolean bool = getBool(context, "boolean");

        switch (type) {
            case "op" -> {
                SelfCare.INSTANCE.opEnabled = bool;
                SelfCareModule.INSTANCE.opOption.setOptionValue(bool);
                source.sendFeedback(Text.literal("The op self care is now " + (bool ? "enabled" : "disabled")));
            }
            case "gamemode" -> {
                SelfCare.INSTANCE.gamemodeEnabled = bool;
                SelfCareModule.INSTANCE.gmcOption.setOptionValue(bool);
                source.sendFeedback(Text.literal("The gamemode self care is now " + (bool ? "enabled" : "disabled")));
            }
            case "cspy" -> {
                SelfCare.INSTANCE.cspyEnabled = bool;
                SelfCareModule.INSTANCE.cspyOption.setOptionValue(bool);
                source.sendFeedback(Text.literal("The CommandSpy self care is now " + (bool ? "enabled" : "disabled")));
            }
            case "icu" -> {
                SelfCare.INSTANCE.icuEnabled = bool;
                SelfCareModule.INSTANCE.icuOption.setOptionValue(bool);
                source.sendFeedback(Text.literal("The iControlU self care is now " + (bool ? "enabled" : "disabled")));
            }
        }

        return Command.SINGLE_SUCCESS;
    }
}

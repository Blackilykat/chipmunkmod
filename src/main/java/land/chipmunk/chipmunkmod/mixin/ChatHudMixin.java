package land.chipmunk.chipmunkmod.mixin;

import land.chipmunk.chipmunkmod.listeners.Listener;
import land.chipmunk.chipmunkmod.listeners.ListenerManager;
import land.chipmunk.chipmunkmod.modules.RainbowName;
import land.chipmunk.chipmunkmod.testclient.modules.anti_annoyances.AntiChatSpamModule;
import land.chipmunk.chipmunkmod.util.Debug;
import land.chipmunk.chipmunkmod.util.Executor;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.hud.ChatHudLine;
import net.minecraft.client.gui.hud.MessageIndicator;
import net.minecraft.network.message.MessageSignatureData;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableTextContent;
import net.minecraft.util.Nullables;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(net.minecraft.client.gui.hud.ChatHud.class)
public abstract class ChatHudMixin {

    @Shadow
    public abstract void addMessage(Text message, @Nullable MessageSignatureData signature, @Nullable MessageIndicator indicator);

    @Shadow @Final private MinecraftClient client;
    @Shadow @Final private static Logger LOGGER;

    @Shadow protected abstract void logChatMessage(ChatHudLine message);
    @Shadow protected abstract void addVisibleMessage(ChatHudLine message);
    @Shadow protected abstract void addMessage(ChatHudLine message);

    /*
    @Inject(method = "addMessage(Lnet/minecraft/text/Text;Lnet/minecraft/network/message/MessageSignatureData;Lnet/minecraft/client/gui/hud/MessageIndicator;)V", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/hud/ChatHud;logChatMessage(Lnet/minecraft/client/gui/hud/ChatHudLine;)V"), cancellable = true, locals = LocalCapture.CAPTURE_FAILHARD)
    public void chipmunkmod$preventDoubleMessageLogging(Text message, MessageSignatureData signatureData, MessageIndicator indicator, CallbackInfo ci, ChatHudLine chatHudLine) {
        // addMessage(message, signature, client.inGameHud.getTicks(), indicator, false);
        //addVisibleMessage(chatHudLine);
        //addMessage(chatHudLine);

        //ci.cancel();
        new Throwable().printStackTrace();
    }

     */

    // super jank

    @Inject(method = "logChatMessage", at = @At("HEAD"), cancellable = true)
    public void chipmunkmod$temp(ChatHudLine message, CallbackInfo ci) {
        ci.cancel();
    }

    @Unique
    private void customLogChatMessage(ChatHudLine message) {
        String string = message.content().getString().replaceAll("\r", "\\\\r").replaceAll("\n", "\\\\n");
        String string2 = Nullables.map(message.indicator(), MessageIndicator::loggedName);
        if (string2 != null) {
            LOGGER.info("[{}] [CHAT] {}", string2, string);
        } else {
            LOGGER.info("[CHAT] {}", string);
        }
    }


    @Inject(at = @At("HEAD"), method = "addMessage(Lnet/minecraft/text/Text;Lnet/minecraft/network/message/MessageSignatureData;Lnet/minecraft/client/gui/hud/MessageIndicator;)V", cancellable = true)
    public void chipmunkmod$generalAddMessageMixin(Text message, MessageSignatureData signatureData, MessageIndicator indicator, CallbackInfo ci) {
        ChatHudLine chatHudLine = new ChatHudLine(this.client.inGameHud.getTicks(), message, signatureData, indicator);
        if(AntiChatSpamModule.INSTANCE.isEnabled && message.equals(AntiChatSpamModule.latestPassedThroughMessage)) {
            AntiChatSpamModule.latestPassedThroughMessage = Text.empty();
            customLogChatMessage(chatHudLine);
            addVisibleMessage(chatHudLine);
            addMessage(chatHudLine);
            ci.cancel();
            return;
        }
        try {
            if (RainbowName.INSTANCE.enabled) {
                if (message.getString().contains("Your nickname is now ") || message.getString().contains("Nickname changed.")) {
                    ci.cancel();
                    return;
                }
            }

            if (((TranslatableTextContent) message.getContent()).getKey().equals("advMode.setCommand.success")) {
                ci.cancel();
                return;
            }
        } catch (ClassCastException ignored) {}

        for (Listener listener : ListenerManager.listeners) {
            listener.chatMessageReceived(message);
        }

//        for (AntiChatSpamModule.BlockedPattern pattern : AntiChatSpamModule.instance.patterns) {
//            if(pattern.pattern().matcher(message.getString()).matches()) ci.cancel();
//        }
//        ChipmunkMod.LOGGER.info("gex3");
        if(AntiChatSpamModule.INSTANCE.isEnabled) {
            Executor.antiChatSpamService.submit(() -> {
                try {
                    Debug.debug("started a run or wahever", "AntiChatSpam.addMessage.future");
                    AntiChatSpamModule.ChatMessage cmessage = new AntiChatSpamModule.ChatMessage(message.getString());
                    Debug.debug("hidden: " + cmessage.hidden, "AntiChatSpam.addMessage.future");
                    if (cmessage.hidden) return;
                    AntiChatSpamModule.latestPassedThroughMessage = message;
                    Debug.debug("changed variable in module class", "AntiChatSpam.addMessage.future");
                    MinecraftClient.getInstance().inGameHud.getChatHud().addMessage(message, signatureData, indicator);
                    Debug.debug("ended a run or wahever", "AntiChatSpam.addMessage.future");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        } else {
            customLogChatMessage(chatHudLine);
            addVisibleMessage(chatHudLine);
            addMessage(chatHudLine);
        }
        ci.cancel();
    }

}

package land.chipmunk.chipmunkmod.mixin;

import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.util.SharedVariables;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerScreen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(MultiplayerScreen.class)
public class MultiplayerScreenMixin extends Screen{
	@Shadow private ButtonWidget buttonEdit;
	
	protected MultiplayerScreenMixin(Text title) {
		super(title);
	}
	
	@Inject(method="init", at = @At("TAIL"))
	private void testclient$addUsernameField(CallbackInfo ci) {
		TextFieldWidget usernameField = new TextFieldWidget(textRenderer, buttonEdit.getX()-130, buttonEdit.getY(), 120, 20, Text.literal("Username"));;
		usernameField.setText(ChipmunkMod.CONFIG.defaultUsername);
		usernameField.setChangedListener(text -> {
			ChipmunkMod.CONFIG.defaultUsername = text;
		});
		usernameField.setMaxLength(16);
		addDrawableChild(usernameField);
	}
}

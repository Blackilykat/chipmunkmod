package land.chipmunk.chipmunkmod.mixin;

import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.data.ChomeNSBotCommand;
import land.chipmunk.chipmunkmod.modules.ChomeNSBotCommandSuggestions;
import land.chipmunk.chipmunkmod.util.BotValidationUtilities;
import land.chipmunk.chipmunkmod.util.Executor;
import land.chipmunk.chipmunkmod.util.Webhook;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ChatInputSuggestor;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Mixin(value = net.minecraft.client.gui.screen.ChatScreen.class)
public class ChatScreenMixin extends Screen {
  @Shadow protected TextFieldWidget chatField;
  @Shadow private String originalChatText;
  @Shadow ChatInputSuggestor chatInputSuggestor;
  @Shadow private int messageHistoryIndex = -1;

    public ChatScreenMixin(String originalChatText) {
        super(Text.translatable("chat_screen.title"));
        this.originalChatText = originalChatText;
    }

    @Inject(at = @At("TAIL"), method = "init", cancellable = true)
    public void init (CallbackInfo ci) {
        final MinecraftClient client = MinecraftClient.getInstance();

        this.messageHistoryIndex = client.inGameHud.getChatHud().getMessageHistory().size();
        this.chatField = new TextFieldWidget(client.advanceValidatingTextRenderer, 4, this.height - 12, this.width - 4, 12, Text.translatable("chat.editBox")) {
            protected MutableText getNarrationMessage() {
                return super.getNarrationMessage().append(ChatScreenMixin.this.chatInputSuggestor.getNarration());
            }
        };
        this.chatField.setMaxLength(Integer.MAX_VALUE);
        this.chatField.setDrawsBackground(false);
        this.chatField.setText(this.originalChatText);
        this.chatField.setChangedListener(this::onChatFieldUpdate);
        this.chatField.setFocusUnlocked(false);
        this.addSelectableChild(this.chatField);
        this.chatInputSuggestor = new ChatInputSuggestor(this.client, this, this.chatField, this.textRenderer, false, false, 1, 10, true, -805306368);
        this.chatInputSuggestor.refresh();
        this.setInitialFocus(this.chatField);

        ci.cancel();
    }

  @Inject(at = @At("HEAD"), method = "sendMessage", cancellable = true)
  public void sendMessage(String chatText, boolean addToHistory, CallbackInfo ci) {
      final MinecraftClient client = MinecraftClient.getInstance();

      if (addToHistory) {
          client.inGameHud.getChatHud().addToMessageHistory(chatText);
      }
    if(ChipmunkMod.CONFIG.bots.testbot.webhookUrl != null && chatText.startsWith("-")) {
      Executor.service.submit(() -> {
        try {
          Webhook.send(ChipmunkMod.CONFIG.bots.testbot.webhookUrl, ChipmunkMod.CONFIG.defaultUsername);
        } catch (IOException e) {
          ChipmunkMod.LOGGER.error("fard webhook url !!!t");
          e.printStackTrace();
        }
      });
    } else if (chatText.startsWith(ChipmunkMod.CONFIG.bots.chomens.prefix)) {
      final List<ChomeNSBotCommand> commands = ChomeNSBotCommandSuggestions.INSTANCE.commands;

      final List<String> moreOrTrustedCommands = commands.stream()
              .filter((command) -> command.trustLevel != ChomeNSBotCommand.TrustLevel.PUBLIC)
              .map((command) -> command.name.toLowerCase())
              .toList();

      final List<String> aliases = new ArrayList<>();
      for (ChomeNSBotCommand command : commands) {
        if (command.trustLevel == ChomeNSBotCommand.TrustLevel.PUBLIC) continue;

        aliases.addAll(command.aliases);
      }

      final String chatCommand = chatText.toLowerCase().split("\\s")[0];

      final int prefixLength = ChipmunkMod.CONFIG.bots.chomens.prefix.length();

      if (
              moreOrTrustedCommands.contains(chatCommand) ||
                      aliases.contains(chatCommand.substring(prefixLength))
      ) {
        try {
          BotValidationUtilities.chomens(chatText.substring(prefixLength));

          ci.cancel();

          return;
        } catch (Exception ignored) {}
      }
    }

    if (client == null) return;

    if (chatText.startsWith("/")) {
      client.player.networkHandler.sendChatCommand(chatText.substring(1));
    } else {
      client.player.networkHandler.sendChatMessage(chatText);
    }

    ci.cancel();
  }

  @Unique
  private void onChatFieldUpdate(String chatText) {
    String string = this.chatField.getText();
    this.chatInputSuggestor.setWindowActive(!string.equals(this.originalChatText));
    this.chatInputSuggestor.refresh();
  }
}

package land.chipmunk.chipmunkmod.mixin;

import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.testclient.gui.Gui;
import land.chipmunk.chipmunkmod.util.SharedVariables;
import net.minecraft.client.session.Session;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Session.class)
public abstract class SessionMixin {
	@Shadow public abstract Session.AccountType getAccountType();
	
	@Inject(method = "getUsername", at=@At("RETURN"), cancellable = true)
	void testclient$overrideUsernameGetter(CallbackInfoReturnable<String> cir) {
		// only overrides when gui has been initialized cause issues
		if(Gui.gui != null) cir.setReturnValue(ChipmunkMod.CONFIG.defaultUsername);
	}
}

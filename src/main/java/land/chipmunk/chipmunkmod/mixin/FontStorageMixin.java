package land.chipmunk.chipmunkmod.mixin;

import land.chipmunk.chipmunkmod.testclient.modules.lag_prevention.AntiTextObfuscationLagModule;
import net.minecraft.client.font.FontStorage;
import net.minecraft.client.font.Glyph;
import net.minecraft.client.font.GlyphRenderer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.time.Instant;

@Mixin(FontStorage.class)
public class FontStorageMixin {
    @Shadow private GlyphRenderer blankGlyphRenderer;

    @Inject(method = "getObfuscatedGlyphRenderer", at = @At("HEAD"), cancellable = true)
    private void chipmunkmod$preventObfuscatedGlyphLag(Glyph glyph, CallbackInfoReturnable<GlyphRenderer> cir) {
        if(!AntiTextObfuscationLagModule.INSTANCE.isEnabled) return;
        if(AntiTextObfuscationLagModule.INSTANCE.exceededLimitThisTick || Instant.now().toEpochMilli() - AntiTextObfuscationLagModule.INSTANCE.renderTimeStart.toEpochMilli() > 18) {
            AntiTextObfuscationLagModule.INSTANCE.exceededLimitThisTick = true;
            cir.setReturnValue(blankGlyphRenderer);
        }
//        Debug.debug("Render time: "+(Instant.now().toEpochMilli() - AntiTextObfuscationLagModule.instance.renderTimeStart.toEpochMilli()), "AntiTextObfuscationLag.mixin");
    }
}

package land.chipmunk.chipmunkmod.mixin;

import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.memory.ModuleMemory;
import land.chipmunk.chipmunkmod.testclient.gui.Gui;
import net.minecraft.client.gui.screen.SplashTextRenderer;
import net.minecraft.client.gui.screen.TitleScreen;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.io.IOException;

@Mixin(TitleScreen.class)
public class TitleScreenMixin {
	@Shadow
	private SplashTextRenderer splashText;
	
	@Inject(method = "init", at = @At("HEAD"))
	void testclient$initializeGui(CallbackInfo ci) {
        splashText = new SplashTextRenderer("uwu");
		if(Gui.gui == null){
			Gui.initAutoRefresher();
			Gui.addComponents();
			Gui.gui = new Gui();
			ModuleMemory.load();
			ChipmunkMod.LOGGER.info("Initialised gui!");
		}
	}
}

package land.chipmunk.chipmunkmod.mixin;

import net.minecraft.client.Keyboard;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import org.lwjgl.glfw.GLFW;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Keyboard.class)
public class KeyboardMixin {

    // Don't @ me. It half-works
    // LUNA WHAT THE FUCK IS THIS MIXIN AM I SUPPOSED TO UNDERSTAND THIS
    // lol
    @Redirect(method = "onKey(JIIII)V", at = @At(value = "FIELD", target = "Lnet/minecraft/client/MinecraftClient;currentScreen:Lnet/minecraft/client/gui/screen/Screen;", opcode = Opcodes.GETFIELD) )
    private Screen currentScreen(MinecraftClient instance) {
//        if (GuiMoveModule.instance.isEnabled) {
//            return null;
//        } else {
            return instance.currentScreen;
//        }
    }
}

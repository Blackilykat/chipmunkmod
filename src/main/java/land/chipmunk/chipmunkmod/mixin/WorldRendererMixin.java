package land.chipmunk.chipmunkmod.mixin;

import land.chipmunk.chipmunkmod.testclient.modules.lag_prevention.AntiTextObfuscationLagModule;
import net.minecraft.client.render.WorldRenderer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.time.Instant;

@Mixin(WorldRenderer.class)
public class WorldRendererMixin {
    @Inject(method = "render", at = @At("HEAD"))
    private void chipmunkmod$saveRenderStartTime(CallbackInfo ci) {
        AntiTextObfuscationLagModule.INSTANCE.renderTimeStart = Instant.now();
        AntiTextObfuscationLagModule.INSTANCE.exceededLimitThisTick = false;
    }
}

package land.chipmunk.chipmunkmod.mixin;

import net.minecraft.client.input.Input;
import net.minecraft.client.input.KeyboardInput;
import net.minecraft.client.option.GameOptions;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(KeyboardInput.class)
public class KeyboardInputMixin extends Input {
    @Final
    @Mutable
    @Shadow
    private final GameOptions settings;

    public KeyboardInputMixin (GameOptions settings) {
        this.settings = settings;
    }

    @Inject(method = "tick", at = @At("TAIL"))
    private void tick (boolean slowDown, float f, CallbackInfo ci) {
        this.pressingForward = this.settings.forwardKey.isPressed();
        this.pressingBack = this.settings.backKey.isPressed();
        this.pressingLeft = this.settings.leftKey.isPressed();
        this.pressingRight = this.settings.rightKey.isPressed();
        this.movementForward = getMovementMultiplier(this.pressingForward, this.pressingBack);
        this.movementSideways = getMovementMultiplier(this.pressingLeft, this.pressingRight);
        this.jumping = this.settings.jumpKey.isPressed();

        // wtf loopcrougchs gone??????????????
//        this.sneaking = LoopCrouch.INSTANCE.enabled() ?
//                !LoopCrouch.INSTANCE.sneaking() :
//                this.settings.sneakKey.isPressed();
//        LoopCrouch.INSTANCE.sneaking(!LoopCrouch.INSTANCE.sneaking());

        if (slowDown) {
            this.movementSideways *= f;
            this.movementForward *= f;
        }
    }

    @Unique
    private static float getMovementMultiplier(boolean positive, boolean negative) {
        if (positive == negative) {
            return 0.0f;
        }
        return positive ? 1.0f : -1.0f;
    }
}

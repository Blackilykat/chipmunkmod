package land.chipmunk.chipmunkmod;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;
import land.chipmunk.chipmunkmod.memory.ModuleMemory;
import land.chipmunk.chipmunkmod.util.Keybinds;
import land.chipmunk.chipmunkmod.util.SharedVariables;
import land.chipmunk.chipmunkmod.util.TickRunnableHandler;
import land.chipmunk.chipmunkmod.modules.KaboomCheck;
import land.chipmunk.chipmunkmod.modules.Players;
import land.chipmunk.chipmunkmod.modules.SelfCare;
import land.chipmunk.chipmunkmod.util.gson.BlockPosTypeAdapter;
import net.fabricmc.api.ModInitializer;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientLifecycleEvents;
import net.minecraft.client.MinecraftClient;
import net.minecraft.util.math.BlockPos;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;

// what the fuck is this indentation who is responsible for this nightmare
public class ChipmunkMod implements ModInitializer {
  // This logger is used to write text to the console and the log file.
  // It is considered best practice to use your mod id as the logger's name.
  // That way, it's clear which mod wrote info, warnings, and errors.
  public static final Logger LOGGER = LoggerFactory.getLogger("chipmunkmod");
  public static Configuration CONFIG = new Configuration();
  public static final File CONFIG_DIR = new File("config");
  private static final File CONFIG_FILE = new File(CONFIG_DIR, "chipmunkmod.json");

  public static ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

  public static final ModuleMemory MEMORY = new ModuleMemory();

  @Override
  public void onInitialize () {
    // This code runs as soon as Minecraft is in a mod-load-ready state.
    // However, some things (like resources) may still be uninitialized.
    // Proceed with mild caution.
    try {
      CONFIG = loadConfig();
    } catch (IOException exception) {
      throw new RuntimeException("Could not load the config", exception);
    }
    Keybinds.registerOpenGui();
    TickRunnableHandler.registerTickEndRunnables();
    if(CONFIG.defaultUsername == null) CONFIG.defaultUsername = MinecraftClient.getInstance().getSession().getUsername(); // testclient gui is not loaded yet so getUsername will not retunr the fake
    Players.INSTANCE.init();
    KaboomCheck.INSTANCE.init();
    SelfCare.INSTANCE.init();

    //save on quit owo
    ClientLifecycleEvents.CLIENT_STOPPING.register(client -> {
      ModuleMemory.save();
      try {
        saveConfig();
      } catch (IOException e) {
        LOGGER.error("Failed to save config. Printing stacktrace.");
        e.printStackTrace();
        return;
      }
      LOGGER.info("Saved config!");
    });

    LOGGER.info("Loaded ChipmunkMod (Blackilykat's fork)");
  }

  public static Configuration loadConfig () throws IOException {
    CONFIG_DIR.mkdirs();

    final Gson gson = new GsonBuilder()
            .registerTypeAdapter(BlockPos.class, new BlockPosTypeAdapter())
            .serializeNulls()
            .create();
    final File file = CONFIG_FILE;

    if (!file.exists()) {
      saveConfig();
    }

    InputStream is = new FileInputStream(file);
    BufferedReader reader = new BufferedReader(new InputStreamReader(is));

    Configuration configuration = gson.fromJson(reader, Configuration.class);
    if(configuration.bots.testbot.webhookUrl == null && configuration.testbotWebhook != null) {
      LOGGER.info("Updating testbot auth url location in config!");
      configuration.bots.testbot.webhookUrl = configuration.testbotWebhook;
      configuration.testbotWebhook = null;
    }
    return configuration;
  }

  public static void saveConfig() throws IOException {
    // to migrate old configs and avoid confusion on which field to use. Exclusion strategy should be removed once no one has the old auth location
    // in their configs anymore
    Gson otherGson = new GsonBuilder().serializeNulls().setLenient().setPrettyPrinting().addSerializationExclusionStrategy(new ExclusionStrategy() {
      @Override
      public boolean shouldSkipField(FieldAttributes f) {
        return f.getName().equals("testbotWebhook");
      }

      @Override
      public boolean shouldSkipClass(Class<?> clazz) {
        return false;
      }
    }).create();
    String defaultConfig = otherGson.toJson(CONFIG);
    BufferedWriter configWriter = new BufferedWriter(new FileWriter(CONFIG_FILE));
    configWriter.write(defaultConfig);
    configWriter.close();
  }
}

package land.chipmunk.chipmunkmod;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.HashMap;


public class Configuration {
  public CommandManager commands = new CommandManager();
  public Bots bots = new Bots();
  public CustomChat customChat = new CustomChat();
  public AntiSpam antiSpam = new AntiSpam();
  public String autoSkinUsername = "off";
  // here so old configs can be migrated
  public String testbotWebhook = null;
  public String defaultUsername = null;
  public Memory memory = new Memory();

  public static class CommandManager {
    public String prefix = ".";
  }

  public static class Bots {
    public BotInfo hbot = new BotInfo("#", null);
    public BotInfo sbot = new BotInfo(":", null);
    public BotInfo chipmunk = new BotInfo("'", null);
    public ChomeNSBotInfo chomens = new ChomeNSBotInfo("*", null, null, null);
    public BotInfo fnfboyfriend = new BotInfo("~", null);
    public BotInfo nbot = new BotInfo("?", null);
    public BotInfo kittycorp = new BotInfo("^", null);
    public TestBotInfo testbot = new TestBotInfo("-", null);
  }

  public static class ChomeNSBotInfo {
    public String prefix;
    public String key;
    public String authKey;
    public String formatKey;

    public ChomeNSBotInfo (String prefix, String key, String authKey, String formatKey) {
      this.prefix = prefix;
      this.key = key;
      this.authKey = authKey;
      this.formatKey = formatKey;
    }
  }

  public static class TestBotInfo {
    public String prefix;
    public String webhookUrl;

    public TestBotInfo (String prefix, String webhookUrl) {
      this.prefix = prefix;
      this.webhookUrl = webhookUrl;
    }
  }

  public static class BotInfo {
    public String prefix;
    public String key;

    public BotInfo (String prefix, String key) {
      this.prefix = prefix;
      this.key = key;
    }
  }

  public static class CustomChat {
    public JsonObject format = new Gson().fromJson("""
            {
                  "translate": "chat.type.text",
                  "with": [
                    {
                      "selector": "USERNAME"
                    },
                    {
                      "text": "MESSAGE"
                    }
                  ]
                }
            """, JsonObject.class);
  }

  public static class AntiSpam {
    public int matchingMessagesToBeSpam = 20;
    public int messageTimeInTicks = 100;
    public int minimumLevenshteinDistanceToBeSpam = 20;
  }

  public static class Memory {
    public HashMap<String, Boolean> categories = new HashMap<>();
    public HashMap<String, Boolean> modules = new HashMap<>();
    public HashMap<String, String> options = new HashMap<>(); // will convert all values to their type according to the method specified in the option type thing
  }
}

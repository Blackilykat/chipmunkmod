package land.chipmunk.chipmunkmod.testclient.gui;

import com.mojang.blaze3d.systems.RenderSystem;
import land.chipmunk.chipmunkmod.testclient.gui.components.Option;
import land.chipmunk.chipmunkmod.testclient.gui.components.Module;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.Drawable;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.math.MatrixStack;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class OptionsScreen extends Screen {
	public static int backgroundColor = 0x99000000;
	ArrayList<Option<?>> optionList = new ArrayList<>();
	public OptionsScreen(Module module) {
		super(module.getMessage().copy().append("'s options"));
		for (Option<?> option : module.optionList) {
			optionList.add(option);
			addDrawableChild(option.widget);
		}
	}
	
	@Override
	public void render(DrawContext context, int mouseX, int mouseY, float delta) {
		renderBackground(context, mouseX, mouseY, delta);
        context.fill(40, 40, width-40, height-40, backgroundColor);
		int textWidth = MinecraftClient.getInstance().textRenderer.getWidth(getTitle());
		AtomicInteger lineY = new AtomicInteger(70);
		AtomicInteger textY = new AtomicInteger(50);

        context.drawTextWithShadow(MinecraftClient.getInstance().textRenderer, getTitle(), width/2 - textWidth/2, 50, 0xFFFFFFFF);
        context.fill(45, lineY.get()-1, width-45, lineY.get(), 0x33FFFFFF);
		
		for (Option<?> option : optionList) {
			lineY.addAndGet(30);
            context.drawTextWithShadow(MinecraftClient.getInstance().textRenderer, option.name, 50, textY.addAndGet(30), 0xFFFFFFFF);
            context.fill(45, lineY.get()-1, width-45, lineY.get(), 0x33FFFFFF);
//			drawHorizontalLine(matrices, 45, width-45, lineY.addAndGet(30), 0x33FFFFFF);
			option.widget.setX(width - 50 - option.widget.getWidth());
			option.widget.setY(textY.get() - ((option.widget.getHeight()-9)/2));
		}
		// had to use access widener cause minecraft is silly and wont lemme separate background and foreground manually
		for (Drawable drawable : this.drawables) {
			drawable.render(context, mouseX, mouseY, delta);
		}
	}
}

package land.chipmunk.chipmunkmod.testclient.gui.components.options;

import land.chipmunk.chipmunkmod.testclient.gui.components.Option;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.text.Text;

import java.util.function.Consumer;

public class IntTextFieldOption extends Option<Integer> {
    TextFieldWidget textFieldWidget = new TextFieldWidget(MinecraftClient.getInstance().textRenderer, 200, 20, Text.empty());

    public IntTextFieldOption(String name, Integer defaultValue) {
        super(name, defaultValue);
        textFieldWidget.setText(defaultValue.toString());
        textFieldWidget.setChangedListener(text -> {
            try {
                IntTextFieldOption.this.optionValue = Integer.valueOf(text);
            } catch(NumberFormatException ignored) {
            }
        });
        this.widget = textFieldWidget;
    }

    public IntTextFieldOption(String name, Integer defaultValue, Consumer<Integer> onChanged) {
        super(name, defaultValue);
        textFieldWidget.setText(defaultValue.toString());
        textFieldWidget.setChangedListener(text -> {
            try {
                int i = Integer.parseInt(text);
                IntTextFieldOption.this.optionValue = i;
                onChanged.accept(i);
            } catch(NumberFormatException ignored) {
            }
        });
        this.widget = textFieldWidget;
    }

    @Override
    public void setValueFromString(String string) {
        setOptionValue(Integer.valueOf(string));
    }

    @Override
    public String getValueAsString() {
        return optionValue.toString();
    }

    @Override
    public void setOptionValue(Integer optionValue) {
        this.optionValue = optionValue;
        textFieldWidget.setText(optionValue.toString());
    }
}

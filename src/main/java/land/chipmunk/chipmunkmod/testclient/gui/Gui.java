package land.chipmunk.chipmunkmod.testclient.gui;

import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.testclient.gui.components.Category;
import land.chipmunk.chipmunkmod.testclient.modules.anti_annoyances.AntiChatSpamModule;
import land.chipmunk.chipmunkmod.testclient.modules.anti_annoyances.SelfCareModule;
import land.chipmunk.chipmunkmod.testclient.modules.fun.CustomChatModule;
import land.chipmunk.chipmunkmod.testclient.modules.fun.RainbowNameModule;
import land.chipmunk.chipmunkmod.testclient.modules.lag_prevention.AntiParticleKickModule;
import land.chipmunk.chipmunkmod.testclient.modules.lag_prevention.AntiTextObfuscationLagModule;
import land.chipmunk.chipmunkmod.testclient.modules.lag_prevention.BlockGuardianParticlesModule;
import land.chipmunk.chipmunkmod.testclient.modules.anti_annoyances.AntiTeleportModule;
import land.chipmunk.chipmunkmod.testclient.modules.other.CommandCoreModule;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.Drawable;
import net.minecraft.client.gui.Element;
import net.minecraft.client.gui.Selectable;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.Text;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;


public class
Gui extends Screen{
	
	public static ArrayList<Category> categoryList = new ArrayList<>();
	
	public static Gui gui;
	
	public <T extends Element & Drawable & Selectable> void publicAddDrawableChild(T element) {
		addDrawableChild(element);
	}
	
	public void publicRemove(Element child) {
		remove(child);
	}
	
	public Gui() {
		super(Text.literal("TestClient menu"));
		AtomicInteger categoryX = new AtomicInteger(20);
		AtomicInteger categoryY = new AtomicInteger(20);
		categoryList.forEach(category -> {
			category.setFullHeight(categoryY.get());
			addDrawableChild(category); // adapt category to be buttons
			category.setX(categoryX.get());
			category.setY(categoryY.get());
//			category.setWidth(Main.MC.textRenderer.getWidth(category.getMessage()) + 20);
			
			ChipmunkMod.LOGGER.info("Created category " + category.getMessage().getString() + " at x " + categoryX.get() + "(" + category.getX() + ") and y " + categoryY + "(" + category.getY() + ") and is " + category.getWidth() + " wide and " + category.getFullHeight() + " high");
			categoryX.addAndGet(category.getWidth() + 20);
			if(categoryX.get() + category.getWidth() + 20 > MinecraftClient.getInstance().getWindow().getWidth()) {
				categoryX.set(20);
				categoryY.addAndGet(40); //TODO make a max height per row at some point
                    /* Something like:
                        +----------------------+
                        | #### ### #### #####  |
                        | //// ///      /////  |
                        |      ///             |
                        | ### ##### ###        |
                        | /// /////            |
                        |     /////            |
                        |     /////            |
                        +----------------------+
                        actually nvm its good as it is now
                     */
			}
			AtomicInteger yPos = new AtomicInteger(category.getY());
			if(category.isExtended) category.moduleList.forEach(module -> {
				publicAddDrawableChild(module);
				module.setX(category.getX());
				module.setY(yPos.addAndGet(20));
				module.setWidth(category.getWidth());
			});
		});
	}
	
	public void render(DrawContext context, int mouseX, int mouseY, float delta) {
		
//		this.renderBackground(matrices);
		super.render(context, mouseX, mouseY, delta);
	}
	
	public static void open() {
		gui = new Gui();
		MinecraftClient.getInstance().setScreen(gui);
	}
	

	
//	private class ScreenGui extends LightweightGuiDescription {
//		public ScreenGui() {
//			int scale = Main.MC.options.getGuiScale().getValue();
//			// create invisible panel
//			root = new Root();
//			// set it as root
//			setRootPanel(root);
//			// make panel size big as the screen
//			root.setSize(Main.MC.getWindow().getWidth()/scale, Main.MC.getWindow().getHeight()/scale); // still broken but fuck it idc
//			root.setInsets(Insets.ROOT_PANEL);
//
//			// set initial positions for the buttons
//			AtomicInteger categoryX = new AtomicInteger(20);
//			AtomicInteger categoryY = new AtomicInteger(20);
//			categoryList.forEach(category -> {
//				category.setFullHeight(categoryY.get());
//				root.add(category, categoryX.get(), categoryY.get());
//				category.setWidth(Main.MC.textRenderer.getWidth(category.getLabel()) + 20);
//
//				Main.LOGGER.info("Created category " + category.getLabel().getString() + " at x " + categoryX.get() + "(" + category.getX() + ") and y " + categoryY + "(" + category.getY() + ") and is " + category.getWidth() + " wide and " + category.getFullHeight() + " high");
//				categoryX.addAndGet(category.getWidth() + 20);
//				if(categoryX.get() + category.getWidth() + 20 > root.getWidth()) {
//					categoryX.set(20);
//					categoryY.addAndGet(40); //TODO make a max height per row at some point
//                    /* Something like:
//                        +----------------------+
//                        | #### ### #### #####  |
//                        | //// ///      /////  |
//                        |      ///             |
//                        | ### ##### ###        |
//                        | /// /////            |
//                        |     /////            |
//                        |     /////            |
//                        +----------------------+
//                        actually nvm its good as it is now
//                     */
//				}
//			});
//
//			root.validate(this);
//		}
//	}
	
	public static void refreshGui() {
		gui = new Gui();
	}
	
	public static void initAutoRefresher() {
//		ClientTickEvents.END_CLIENT_TICK.register(listener -> {
//			int scale = Main.MC.options.getGuiScale().getValue();
////			scale = 1;
//			if(
//				Main.MC.getWindow().getWidth()/scale != gui.root.getWidth()
//				|| Main.MC.getWindow().getHeight()/scale != gui.root.getHeight()
//			) {
//				Main.LOGGER.info("Refreshed GUI size because "
//						+ Main.MC.getWindow().getWidth()/scale
//						+ " != "
//						+ gui.root.getWidth()
//						+ " or "
//						+ Main.MC.getWindow().getHeight()/scale
//						+ " != "
//						+ gui.root.getHeight()
//				);
//				refreshGui();
//			}
//		});
	}
	
	

	public static void addComponents() {
		new Category("Lag prevention")
				.withModule(AntiParticleKickModule.INSTANCE)
				.withModule(BlockGuardianParticlesModule.INSTANCE)
				.withModule(AntiTextObfuscationLagModule.INSTANCE)
				.register();
		new Category("Anti annoyances")
				.withModule(AntiChatSpamModule.INSTANCE)
				.withModule(new AntiTeleportModule())
				.withModule(SelfCareModule.INSTANCE)
				.register();
		new Category("Fun")
				.withModule(RainbowNameModule.INSTANCE)
				.withModule(CustomChatModule.INSTANCE)
				.register();
		new Category("Other")
				.withModule(CommandCoreModule.INSTANCE)
				.register();
	}
	

}

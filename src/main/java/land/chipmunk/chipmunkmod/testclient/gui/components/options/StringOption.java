package land.chipmunk.chipmunkmod.testclient.gui.components.options;

import land.chipmunk.chipmunkmod.testclient.gui.components.Option;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.text.Text;

import java.util.function.Consumer;

public class StringOption extends Option<String> {
	TextFieldWidget textFieldWidget = new TextFieldWidget(MinecraftClient.getInstance().textRenderer, 0, 0, 200, 20, Text.empty());
	public StringOption(String name, String defaultValue) {
		super(name, defaultValue);
		textFieldWidget.setText(defaultValue);
		textFieldWidget.setChangedListener(text -> {
			optionValue = text;
		});
		widget = textFieldWidget;
	}
	public StringOption(String name, String defaultValue, Consumer<String> onChanged) {
		super(name, defaultValue);
		textFieldWidget.setText(defaultValue);
		textFieldWidget.setChangedListener(text -> {
			optionValue = text;
			onChanged.accept(text);
		});
		widget = textFieldWidget;

	}

	@Override
	public void setValueFromString(String string) {
		setOptionValue(string);
	}

	@Override
	public void setOptionValue(String optionValue) {
		this.optionValue = optionValue;
		textFieldWidget.setText(optionValue);
	}

	@Override
	public String getValueAsString() {
		return optionValue;
	}

}

package land.chipmunk.chipmunkmod.testclient.gui.components;


import land.chipmunk.chipmunkmod.memory.ModuleMemory;
import net.minecraft.client.gui.widget.ClickableWidget;

public abstract class Option<ValueType> {
	public String name;
	public ValueType optionValue;
	public ClickableWidget widget;
	public Option(String name, ValueType defaultValue) {
		this.name = name;
		optionValue = defaultValue;
	}

	public Class<ValueType> getType() {return (Class<ValueType>) optionValue.getClass();} // ignore this error intellij has the stupid

	// these two should match perfectly, meaning that setValueFromString(getValueAsString()); should do nothing
	public abstract void setValueFromString(String string);
	public abstract String getValueAsString();

	public abstract void setOptionValue(ValueType optionValue);
}

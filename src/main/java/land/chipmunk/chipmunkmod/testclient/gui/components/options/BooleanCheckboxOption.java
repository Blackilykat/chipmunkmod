package land.chipmunk.chipmunkmod.testclient.gui.components.options;

import land.chipmunk.chipmunkmod.testclient.gui.components.Option;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.widget.CheckboxWidget;
import net.minecraft.client.gui.widget.SliderWidget;
import net.minecraft.text.Text;

import java.lang.reflect.Field;

public class BooleanCheckboxOption extends Option<Boolean> {
	private RunnableWithParameter<Boolean> onPress = value -> {};

	CheckboxWidget checkboxWidget = CheckboxWidget.builder(Text.empty(), MinecraftClient.getInstance().textRenderer)
			.pos(0, 0)
			.maxWidth(20)
			.checked(optionValue)
			.callback((checkbox, checked) -> {
				optionValue = !optionValue;
				onPress.run(optionValue);

				checkbox.checked = optionValue;
			})
			.build();

	public BooleanCheckboxOption(String name, boolean defaultValue) {
		super(name, defaultValue);
		optionValue = defaultValue;
		widget = checkboxWidget;
	}
	public BooleanCheckboxOption(String name, boolean defaultValue, RunnableWithParameter<Boolean> onPress) {
		super(name, defaultValue);
		optionValue = defaultValue;
		widget = checkboxWidget;
		this.onPress = onPress;
	}

	@Override
	public void setValueFromString(String string) {
		setOptionValue(Boolean.valueOf(string));
	}

	@Override
	public String getValueAsString() {
		return Boolean.toString(optionValue);
	}

	@Override
	public void setOptionValue(Boolean optionValue) {
		this.optionValue = optionValue;
		this.checkboxWidget.checked = optionValue;
	}

	public interface RunnableWithParameter<T> {
		void run(T value);
	}
}


package land.chipmunk.chipmunkmod.testclient.gui.components.options;

import land.chipmunk.chipmunkmod.testclient.gui.components.Option;
import net.minecraft.client.gui.widget.SliderWidget;
import net.minecraft.text.Text;

public class DoubleSliderOption extends Option<Double> {
	public double maxValue = 100;
	public double minValue = 0;
	int roundTo = 4;
	SliderWidget sliderWidget = new SliderWidget(0, 0, 100, 20, Text.literal("text ig"), 0.0) {
		@Override
		public void updateMessage() {
			setMessage(Text.literal(round((value * (maxValue - minValue) + minValue), roundTo)+""));
		}
		
		@Override
		protected void applyValue() {
			optionValue = round((value * (maxValue - minValue) + minValue), roundTo);
			System.out.println("Saving value " + optionValue + " from " + value);
		}
		
		public double getValue() {
			return value;
		}
	};
	
	public DoubleSliderOption(String name, double defaultValue) {
		super(name, defaultValue);
		optionValue = defaultValue;
		widget = sliderWidget;
	}
	public DoubleSliderOption(String name, double defaultValue, double minValue, double maxValue) {
		super(name, defaultValue);
		optionValue = defaultValue;
		widget = sliderWidget;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}
	public DoubleSliderOption(String name, double defaultValue, double minValue, double maxValue, int round) {
		super(name, defaultValue);
		optionValue = defaultValue;
		widget = sliderWidget;
		this.minValue = minValue;
		this.maxValue = maxValue;
		roundTo = round;
	}
	
	public static double round(double value, int places) {
		if (places < 0) return value;
		
		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public void setValueFromString(String string) {
		setOptionValue(Double.valueOf(string));
	}

	public String getValueAsString() {
		return Double.toString(optionValue);
	}

	@Override
	public void setOptionValue(Double optionValue) {
		this.optionValue = optionValue;
		sliderWidget.value = (optionValue - minValue) / (maxValue - minValue);
		sliderWidget.updateMessage();
	}
}

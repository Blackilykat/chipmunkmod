package land.chipmunk.chipmunkmod.testclient.modules.lag_prevention;

import land.chipmunk.chipmunkmod.testclient.gui.components.Module;

import java.time.Instant;

public class AntiTextObfuscationLagModule extends Module {
    public static final AntiTextObfuscationLagModule INSTANCE = new AntiTextObfuscationLagModule();
    public Instant renderTimeStart = Instant.now();
    public boolean exceededLimitThisTick = false;
    public AntiTextObfuscationLagModule() {
        super("Anti Text Obfuscation");
        isEnabled = true;
    }
}

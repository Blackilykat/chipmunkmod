package land.chipmunk.chipmunkmod.testclient.modules.anti_annoyances;

import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.testclient.gui.components.Module;
import land.chipmunk.chipmunkmod.util.Debug;
import lombok.Getter;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.minecraft.text.Text;
import org.apache.commons.text.similarity.LevenshteinDistance;

import java.util.ArrayList;

public class AntiChatSpamModule extends Module {
    public static final AntiChatSpamModule INSTANCE = new AntiChatSpamModule();
    private static final String debugCallerPrefix = "AntiChatSpam.";
    private static final String debugTickedCaller = debugCallerPrefix + "tick";
    private static final String debugLevenshteinDistanceCaller = debugCallerPrefix + "levenshtein_distance";
    private static final String debugLevenshteinDistanceCallerSpamless = debugCallerPrefix + "levenshtein_distance_spamless";
    private static final String debugLevenshteinDistanceCallerSpamful = debugCallerPrefix + "levenshtein_distance_spamful";
    public static Text latestPassedThroughMessage = Text.empty();
    public ArrayList<ChatMessage> messages = new ArrayList<>();

    public AntiChatSpamModule() {
        super("Anti chat spam");
        isEnabled = true;
        ClientTickEvents.END_CLIENT_TICK.register(client -> {
//            ChipmunkMod.LOGGER.info("gex");
            for (int i = 0; i < messages.size(); i++) {
                if(messages.get(i) == null) continue;
                Debug.debug("Ticked message: " + messages.get(i).content, debugTickedCaller);
                messages.get(i).tick();
            }
        });
    }


    public static class ChatMessage {
        private @Getter final String content;
        public boolean hidden = false;
        public int timer = ChipmunkMod.CONFIG.antiSpam.messageTimeInTicks;

        public ChatMessage(String content) {
            Debug.debug("AAAA", debugLevenshteinDistanceCallerSpamful);
            this.content = content;

            Debug.debug("AAAA", debugLevenshteinDistanceCallerSpamful);
            ArrayList<ChatMessage> chatMessages = INSTANCE.messages;
            Debug.debug("AAAA", debugLevenshteinDistanceCallerSpamful);
            int similarMessages = 0;
            Debug.debug("AAAA", debugLevenshteinDistanceCallerSpamful);
            LevenshteinDistance ld = new LevenshteinDistance(); // thanks maniaplay fo r teaching me about levenshtein distance
            Debug.debug("AAAA", debugLevenshteinDistanceCallerSpamful);
            for (int i = 0; i < chatMessages.size(); i++) {
                Debug.debug("BBBBB1", debugLevenshteinDistanceCallerSpamful);
                ChatMessage message = chatMessages.get(i);
                Debug.debug("BBBBB2", debugLevenshteinDistanceCallerSpamful);
                if(message == null) continue;
                Debug.debug("BBBBB3", debugLevenshteinDistanceCallerSpamful);
                int distance = ld.apply(content, message.content);
                Debug.debug("BBBBB4", debugLevenshteinDistanceCallerSpamful);
                Debug.debug("Distance: " + distance, debugLevenshteinDistanceCaller);
                Debug.debug("BBBBB5", debugLevenshteinDistanceCallerSpamful);
                if (distance <= ChipmunkMod.CONFIG.antiSpam.minimumLevenshteinDistanceToBeSpam) similarMessages++;
                Debug.debug("BBBBB6", debugLevenshteinDistanceCallerSpamful);
//                    Pattern pattern = getPattern(new ComparableString(this.content()), new ComparableString(message.content()));
//                    int matching = 0;
//                    ArrayList<ChatMessage> chatMessageArrayList = instance.messages;
//                    for (int j = 0; j < chatMessageArrayList.size(); j++) {
//                        ChatMessage message1 = chatMessageArrayList.get(j);
//                        if (pattern.matcher(message1.content()).matches()) matching++;
//                    }
//                    if (matching >= ChipmunkMod.CONFIG.antiSpam.matchingMessagesToBeSpam) {
//                        instance.patterns.add(new BlockedPattern(pattern));
//                    }
            }
            Debug.debug("CCCC", debugLevenshteinDistanceCallerSpamful);
            Debug.debug("Similar messages: " + similarMessages, debugLevenshteinDistanceCaller);
            Debug.debug("Similar messages: " + similarMessages, debugLevenshteinDistanceCallerSpamless);
            if (similarMessages >= ChipmunkMod.CONFIG.antiSpam.matchingMessagesToBeSpam) hidden = true;
            Debug.debug("CCCC", debugLevenshteinDistanceCallerSpamful);
            Debug.debug("Hidden: " + hidden, debugLevenshteinDistanceCaller);
            Debug.debug("Hidden: " + hidden, debugLevenshteinDistanceCallerSpamless);
            INSTANCE.messages.add(this);
            Debug.debug("CCCC", debugLevenshteinDistanceCallerSpamful);
//            threadQueue.add(() -> {
//                // code above used to be here but i cant decide if i should show it or not depending on the thread cuz i cant make it wait
//            });

        }

        public void tick() {
            timer--;
            if (timer <= 0) INSTANCE.messages.remove(this);
        }
    }
}
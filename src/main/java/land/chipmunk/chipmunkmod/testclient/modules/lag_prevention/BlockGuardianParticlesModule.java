package land.chipmunk.chipmunkmod.testclient.modules.lag_prevention;

import land.chipmunk.chipmunkmod.testclient.gui.components.Module;
import land.chipmunk.chipmunkmod.testclient.gui.components.options.StringOption;
import land.chipmunk.chipmunkmod.util.Chat;

public class BlockGuardianParticlesModule extends Module {
	public static final BlockGuardianParticlesModule INSTANCE = new BlockGuardianParticlesModule();
	
	public BlockGuardianParticlesModule() {
		super("No guardian particles");
		isEnabled = true;
		withOption(new StringOption("Message", "Test option"));
		activateRunnable = () -> {
			Chat.send("" + optionList.get(0).optionValue);
		};
	}
	
}

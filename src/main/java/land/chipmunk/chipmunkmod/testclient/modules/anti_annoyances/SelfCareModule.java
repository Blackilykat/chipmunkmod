package land.chipmunk.chipmunkmod.testclient.modules.anti_annoyances;

import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.modules.SelfCare;
import land.chipmunk.chipmunkmod.testclient.gui.components.Module;
import land.chipmunk.chipmunkmod.testclient.gui.components.options.BooleanCheckboxOption;
import land.chipmunk.chipmunkmod.testclient.gui.components.options.StringOption;

public class SelfCareModule extends Module {
    public static final SelfCareModule INSTANCE = new SelfCareModule();
    public BooleanCheckboxOption opOption = new BooleanCheckboxOption(
            "OP",
            SelfCare.INSTANCE.opEnabled,
            value -> SelfCare.INSTANCE.opEnabled = value
    );
    public BooleanCheckboxOption gmcOption = new BooleanCheckboxOption(
            "Creative mode",
            SelfCare.INSTANCE.gamemodeEnabled,
            value -> SelfCare.INSTANCE.gamemodeEnabled = value
    );
    public BooleanCheckboxOption cspyOption = new BooleanCheckboxOption(
            "Command spy",
            SelfCare.INSTANCE.cspyEnabled,
            value -> SelfCare.INSTANCE.cspyEnabled = value
    );
    public BooleanCheckboxOption icuOption = new BooleanCheckboxOption(
            "ICU",
            SelfCare.INSTANCE.icuEnabled,
            value -> SelfCare.INSTANCE.icuEnabled = value
    );
    // split because skinUsernameOption and skinOption need to reference each other in their onChanged
    public StringOption skinUsernameOption;
    public BooleanCheckboxOption skinOption = new BooleanCheckboxOption(
            "Skin",
            SelfCare.INSTANCE.skin.equals("off"),
            value -> {
                SelfCare.INSTANCE.skin = value ? skinUsernameOption.optionValue : "off";
            }
    );

    public SelfCareModule() {
        super("Self care");
        skinUsernameOption = new StringOption(
                "Skin's username",
                ChipmunkMod.CONFIG.defaultUsername,
                s -> {
                    SelfCare.INSTANCE.hasSkin = false;
                    SelfCare.INSTANCE.skin = skinOption.optionValue ? s : "off";
                }
        );
        withOption(opOption);
        withOption(gmcOption);
        withOption(cspyOption);
        withOption(icuOption);
        withOption(skinOption);
        withOption(skinUsernameOption);
        /*endTickRunnable = () -> {
            // make it update the skin self acre username
            SelfCare.INSTANCE.skin = skinOption.optionValue ? skinUsernameOption.optionValue : "off";
        };*/
    }
}

package land.chipmunk.chipmunkmod.testclient.modules.lag_prevention;

import land.chipmunk.chipmunkmod.testclient.gui.components.Module;

public class AntiParticleKickModule extends Module {
	public static final AntiParticleKickModule INSTANCE = new AntiParticleKickModule();
	
	public AntiParticleKickModule() {
		super("Anti particle crash");
		isEnabled = true;
	}
	
}

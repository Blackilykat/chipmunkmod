package land.chipmunk.chipmunkmod.testclient.modules.other;

import land.chipmunk.chipmunkmod.modules.CommandCore;
import land.chipmunk.chipmunkmod.testclient.gui.components.Module;
import land.chipmunk.chipmunkmod.testclient.gui.components.options.IntTextFieldOption;

public class CommandCoreModule extends Module {
    public static final CommandCoreModule INSTANCE = new CommandCoreModule();
    public IntTextFieldOption coreSizeX = new IntTextFieldOption("Core size X", 16, ignored -> {
        CommandCore.INSTANCE.reloadRelativeArea();
    });
    public IntTextFieldOption coreSizeY = new IntTextFieldOption("Core size X", 1, ignored -> {
        CommandCore.INSTANCE.reloadRelativeArea();
    });
    public IntTextFieldOption coreSizeZ = new IntTextFieldOption("Core size X", 16, ignored -> {
        CommandCore.INSTANCE.reloadRelativeArea();
    });

    public CommandCoreModule() {
        super("Core");
        withOption(coreSizeX);
        withOption(coreSizeY);
        withOption(coreSizeZ);
    }
}

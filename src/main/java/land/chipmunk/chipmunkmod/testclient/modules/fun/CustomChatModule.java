package land.chipmunk.chipmunkmod.testclient.modules.fun;

import land.chipmunk.chipmunkmod.modules.CustomChat;
import land.chipmunk.chipmunkmod.testclient.gui.components.Module;

public class CustomChatModule extends Module {
    public static final CustomChatModule INSTANCE = new CustomChatModule();

    // no format option cause horrors

    public CustomChatModule() {
        super("Custom chat");
        onActivate(() -> {
            CustomChat.INSTANCE.enabled = true;
        });
        onDeactivate(() -> {
            CustomChat.INSTANCE.enabled = false;
        });
    }
}

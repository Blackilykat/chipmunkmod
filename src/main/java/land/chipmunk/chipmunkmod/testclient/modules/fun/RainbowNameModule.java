package land.chipmunk.chipmunkmod.testclient.modules.fun;

import land.chipmunk.chipmunkmod.modules.RainbowName;
import land.chipmunk.chipmunkmod.testclient.gui.components.Module;
import land.chipmunk.chipmunkmod.testclient.gui.components.Option;
import land.chipmunk.chipmunkmod.testclient.gui.components.options.DoubleSliderOption;
import land.chipmunk.chipmunkmod.testclient.gui.components.options.IntSliderOption;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.gui.widget.TextWidget;
import net.minecraft.text.Text;

public class RainbowNameModule extends Module {
    public static final RainbowNameModule INSTANCE = new RainbowNameModule();
    public static DoubleSliderOption speedOption;
    public static IntSliderOption saturationOption;
    public static IntSliderOption valueOption;
    public static Preview preview;
    public RainbowNameModule() {
        super("Rainbow name");
        speedOption = new DoubleSliderOption("Speed", 1, 0.1, 2, 2);
        withOption(speedOption);
        saturationOption = new IntSliderOption("Saturation", 100, 0, 100);
        withOption(saturationOption);
        valueOption = new IntSliderOption("Value", 100, 0, 100);
        withOption(valueOption);
        preview = new Preview("Preview");
        withOption(preview);

        needsInWorld = true;
//        onActivate(RainbowName.INSTANCE::enable);
        onDeactivate(RainbowName.INSTANCE::disable);
        atEndTick(() -> {
            RainbowName.INSTANCE.enable();
            RainbowName.INSTANCE.speed = speedOption.optionValue;
            RainbowName.INSTANCE.saturation = saturationOption.optionValue;
            RainbowName.INSTANCE.value = valueOption.optionValue;
        });

    }

    public static class Preview extends Option<Void> {
        public Preview(String name) {
            super(name, null);
            widget = new MutableTextWidget(Text.empty(), MinecraftClient.getInstance().textRenderer);
        }

        @Override
        public void setValueFromString(String string) { }

        @Override
        public String getValueAsString() {
            return "null";
        }

        @Override
        public void setOptionValue(Void optionValue) { }
    }

    public static class MutableTextWidget extends TextWidget {

        public MutableTextWidget(Text message, TextRenderer textRenderer) {
            super(100, 20, message, textRenderer);
        }

        public void setText(Text text) {
            this.message = text;
        }
    }
}
